#!/bin/bash

myfunc(){
   local myresult=$(( ${1} * ${2} ))
   echo $myresult	 
}

# Later we need to actually call the function.
ANS="$(myfunc 3 2)"

echo $ANS

# Lets do a bunch of stuff!
for i in $(seq 1 100000)
do 
  ANS2=$(myfunc $i $i )
  echo "ANS2 = $ANS2"
done

