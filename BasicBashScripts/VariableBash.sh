#!/bin/bash

# A script with a variable 
# Convention in a bash script is to use Captial for variables.
# This is not required. 

TEXT="My cool text."
NOTCOOL="My not cool text."

echo "Some stuff"
echo $TEXT
echo "Some other stuff ${NOTCOOL}" 
