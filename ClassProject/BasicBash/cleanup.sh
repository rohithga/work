#!/bin/bash

# Removing files with ext. .aux and .log 

# prints out the files
yourfilenames=' ./*.aux ./*.log'

for eachfile in $yourfilenames
do
       	echo $eachfile
done

#asks user for delete conformation
for eachfile in $yourfilenames
do
	echo "Remove file  "$eachfile" ? [y/n]"
	read input

	if [ "$input" == "y" ];
	then
		rm $eachfile
		echo $eachfile "deleted"
	else
		echo $eachfile "not deleted"
	fi
done 
